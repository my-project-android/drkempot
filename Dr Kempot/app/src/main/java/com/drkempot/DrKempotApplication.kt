package com.drkempot

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class DrKempotApplication: Application() {
    override fun onCreate() {
        super.onCreate()
    }
}
