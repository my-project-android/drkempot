package com.drkempot.core.customview

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Gravity
import android.widget.LinearLayout
import com.drkempot.core.R

class CaptureImageDialogView(context: Context) : Dialog(context) {

    private var dialog: Dialog? = null
    private var btnOpenCamera: DrKempotButtonView? = null
    private var btnOpenGallery: DrKempotButtonView? = null

    fun createDialog() {
        dialog = Dialog(context)
        val view = layoutInflater.inflate(R.layout.dialog_capture_image, null)
        dialog?.setCancelable(true)
        dialog?.setContentView(view)
        val window = dialog?.window
        window?.setGravity(Gravity.CENTER)
        window?.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
        )
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        btnOpenCamera = dialog?.findViewById(R.id.btnOpenCamera)
        btnOpenGallery = dialog?.findViewById(R.id.btnOpenGallery)
        dialog?.show()
    }

    fun openCamera(): DrKempotButtonView? {
        return btnOpenCamera
    }

    fun openGallery(): DrKempotButtonView? {
        return btnOpenGallery
    }
}
