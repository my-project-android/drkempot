package com.drkempot.core.constant

object StringConstant {

    const val INTERNET_ISSUE_MESSAGE =
        "Mohon maaf, tidak dapat tersambung ke internet. Silakan periksa koneksi Anda dan coba lagi."

    const val GENERAL_ERROR_MESSAGE = "Terjadi kesalahan. Silakan coba lagi"

    const val WEIGH_BRIDGE_UPLOADER = "WEIGH_BRIDGE_UPLOADER"
    const val DEFAULT_COUNTDOWN = "00:00:00"
}