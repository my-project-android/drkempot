package com.drkempot.core.utils

object Constants {
    const val REFRESH_KEY = "REFRESH KEY"
    const val REFRESH_ARGUMENT = "REFRESH_ARGUMENT"
}