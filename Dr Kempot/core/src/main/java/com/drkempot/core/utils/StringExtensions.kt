package com.drkempot.core.utils

import com.drkempot.core.R

fun String.isPhoneValid(need08: Boolean = true): Boolean {
    val pattern = if (need08) "^08[1-9][0-9]{6,10}\$".toRegex() else "^[1-9][0-9]{6,10}\$".toRegex()
    return matches(pattern)
}

fun String.isNikValid(isValid: Boolean = true): Boolean {
    val pattern = if (isValid) "^[1-16][0-16]{6,16}\$".toRegex() else "".toRegex()
    return matches(pattern)
}

// Need to update below expression to '{0,1}' can be simplified to '?'
fun String.isNameValid(): Boolean = this.matches("^[a-zA-Z]+(\\s[a-zA-Z]+)*\$".toRegex())
fun String.isTextOnlyValid(): Boolean = this.matches("^[a-zA-Z ]*\$".toRegex())

fun String.getPhoneValidationErrorMessage(): Int? {
    val isPrefix08 = matches("^08[0-9]*\$".toRegex())
    return when{
        !isPrefix08 -> R.string.prefix_with_08
        isPrefix08 && !this.isPhoneValid() -> R.string.error_phone_number
        else -> null
    }
}

fun String.getNikValidationErrorMessage(): Int? {
    return when{
        !this.isNikValid() -> R.string.error_nik_number
        else -> null
    }
}