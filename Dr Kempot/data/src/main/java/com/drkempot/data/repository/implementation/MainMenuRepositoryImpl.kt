package com.drkempot.data.repository.implementation

import MainMenuResponse
import com.drkempot.data.repository.interfaces.MainMenuRepository
import com.drkempot.data.source.local.PreferenceHelper
import com.drkempot.data.source.remote.service.MainMenuService
import com.drkempot.data.source.remote.service.Result
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow

class MainMenuRepositoryImpl(
    private val apiService: MainMenuService,
    private val preferenceHelper: PreferenceHelper
) : MainMenuRepository {

    override fun getMainMenu(): Flow<Result<MainMenuResponse>> = flow {
        val response = apiService.getMainMenu()
        emit(Result.createSuccess(data = response, preference = preferenceHelper))
    }.catch { error ->
        emit(
            Result.createError(
                label = "getMainMenu",
                body = null,
                error = error
            )
        )
    }
}