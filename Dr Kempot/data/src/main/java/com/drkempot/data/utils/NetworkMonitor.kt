package com.drkempot.data.utils

interface NetworkMonitor {
    val isConnected: Boolean
}