package com.drkempot.data.domain

data class RegisterResponse(
    var data: RegisterDataResponse,
    var access_token: String,
    var token_type: String
)

data class RegisterDataResponse(
    var name: String,
    var role: String,
    var ktp: String,
    var tanggal: String,
    var id: String,
    var updated_at: String,
    var created_at: String,
)
