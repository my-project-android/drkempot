package com.drkempot.data.repository.implementation

import com.drkempot.data.domain.LoginResponse
import com.drkempot.data.domain.RegisterResponse
import com.drkempot.data.repository.interfaces.AuthRepository
import com.drkempot.data.source.local.PreferenceHelper
import com.drkempot.data.source.remote.payload.LoginRequest
import com.drkempot.data.source.remote.service.AuthService
import com.drkempot.data.source.remote.service.Result
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import okhttp3.RequestBody

class AuthRepositoryImpl(
    private val apiAuthService: AuthService,
    private val preferenceHelper: PreferenceHelper
) : AuthRepository {
    override fun register(registerRequest: RequestBody): Flow<Result<RegisterResponse>> = flow {
        val response = apiAuthService.register(registerRequest)
        emit(Result.createSuccess(data = response, preference = preferenceHelper))
    }.catch { error ->
        emit(
            Result.createError(
                label = "error",
                body = registerRequest,
                error = error
            )
        )
    }

    override fun login(loginRequest: LoginRequest): Flow<Result<LoginResponse>> = flow {
        val response = apiAuthService.login(loginRequest)
        emit(Result.createSuccess(data = response, preference = preferenceHelper))
    }.catch { error ->
        emit(
            Result.createError(
                label = "error",
                body = loginRequest,
                error = error
            )
        )
    }
}