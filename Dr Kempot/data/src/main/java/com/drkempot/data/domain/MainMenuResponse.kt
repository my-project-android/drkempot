data class MainMenuResponse(
    var message: String,
    var user: UserResp,
    var detailuser: DetailUserResp,
    var daftarperiksa: DaftarPeriksaResp,
    var antriansekarang: AntrianSekarangResp
)

data class UserResp(
    var id: String,
    var name: String,
    var email: String,
    var email_verified_at: String,
    var ktp: String,
    var role: String,
    var tanggal: String,
    var created_at: String,
    var updated_at: String
)

data class DetailUserResp(
    var id: String,
    var user_id: String,
    var nohp: String,
    var bpjs: String,
    var fotoktp: String,
    var fotobpjs: String,
    var namakepalakeluarga: String,
    var alamat: String,
    var created_at: String,
    var updated_at: String
)

data class DaftarPeriksaResp(
    var id: String,
    var user_id: String,
    var tanggal: String,
    var noantrian: String,
    var status: String,
    var bpjs: String,
    var created_at: String,
    var updated_at: String
)

data class AntrianSekarangResp(
    var id: String,
    var number: String,
    var created_at: String,
    var updated_at: String
)