package com.drkempot.data.source.remote.payload

data class LoginRequest(
    var ktp: String,
    var password: String
)
