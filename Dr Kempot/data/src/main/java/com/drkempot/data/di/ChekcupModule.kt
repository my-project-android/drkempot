package com.drkempot.data.di

import com.drkempot.data.repository.implementation.CheckupRepositoryImpl
import com.drkempot.data.repository.interfaces.CheckupRepository
import com.drkempot.data.source.local.PreferenceHelper
import com.drkempot.data.source.remote.service.CheckupService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ChekcupModule {

    @Singleton
    @Provides
    fun provideCheckupRepository(
        service: CheckupService,
        preferenceHelper: PreferenceHelper
    ): CheckupRepository{
        return CheckupRepositoryImpl(service, preferenceHelper)
    }
}
