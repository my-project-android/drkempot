package com.drkempot.data.repository.interfaces

import com.drkempot.data.domain.CekAntrianResponse
import com.drkempot.data.domain.JadwalPraktekResponse
import com.drkempot.data.domain.MendaftarPeriksaResponse
import com.drkempot.data.domain.RiwayatPeriksaResponse
import com.drkempot.data.source.remote.payload.CekAntrianRequest
import com.drkempot.data.source.remote.payload.MendaftarPeriksaRequest
import com.drkempot.data.source.remote.service.Result
import kotlinx.coroutines.flow.Flow

interface CheckupRepository {

    fun cekAntrian(
        cekAntrianRequest: CekAntrianRequest
    ): Flow<Result<CekAntrianResponse>>

    fun mendaftarPeriksa(
        mendaftarPeriksaRequest: MendaftarPeriksaRequest
    ): Flow<Result<MendaftarPeriksaResponse>>

    fun riwayatPeriksa(
        filter: String = ""
    ): Flow<Result<RiwayatPeriksaResponse>>

    fun jadwalPraktek(): Flow<Result<JadwalPraktekResponse>>

}
