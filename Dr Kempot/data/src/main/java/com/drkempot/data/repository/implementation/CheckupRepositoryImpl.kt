package com.drkempot.data.repository.implementation

import com.drkempot.data.domain.CekAntrianResponse
import com.drkempot.data.domain.JadwalPraktekResponse
import com.drkempot.data.domain.MendaftarPeriksaResponse
import com.drkempot.data.domain.RiwayatPeriksaResponse
import com.drkempot.data.repository.interfaces.CheckupRepository
import com.drkempot.data.source.local.PreferenceHelper
import com.drkempot.data.source.remote.payload.CekAntrianRequest
import com.drkempot.data.source.remote.payload.MendaftarPeriksaRequest
import com.drkempot.data.source.remote.service.CheckupService
import com.drkempot.data.source.remote.service.Result
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow

class CheckupRepositoryImpl(
    private val apiService: CheckupService,
    private val preferenceHelper: PreferenceHelper
) : CheckupRepository {
    override fun cekAntrian(cekAntrianRequest: CekAntrianRequest): Flow<Result<CekAntrianResponse>> =
        flow {
            val response = apiService.cekAntrian(cekAntrianRequest)
            emit(Result.createSuccess(data = response, preference = preferenceHelper))
        }.catch { error ->
            emit(
                Result.createError(
                    label = "Cek Antrian",
                    body = cekAntrianRequest,
                    error = error
                )
            )
        }

    override fun mendaftarPeriksa(mendaftarPeriksaRequest: MendaftarPeriksaRequest):
            Flow<Result<MendaftarPeriksaResponse>> = flow {
        val response = apiService.mendaftarPeriksa(mendaftarPeriksaRequest)
        emit(Result.createSuccess(data = response, preference = preferenceHelper))
    }.catch { error ->
        emit(
            Result.createError(
                label = "Mendaftar Periksa",
                body = mendaftarPeriksaRequest,
                error = error
            )
        )
    }

    override fun riwayatPeriksa(filter: String): Flow<Result<RiwayatPeriksaResponse>> = flow {
        val query = mapOf(
            "filter" to filter
        ).filterValues {
            it.isNotBlank()
        }

        val response = apiService.riwayatPeriksa(query)
        emit(Result.createSuccess(data = response, preference = preferenceHelper))
    }.catch { error ->
        emit(
            Result.createError(
                label = "Riwayat Periksa",
                body = filter,
                error = error
            )
        )
    }

    override fun jadwalPraktek(): Flow<Result<JadwalPraktekResponse>> = flow {
        val response = apiService.jadwalPraktek()
        emit(Result.createSuccess(data = response, preference = preferenceHelper))
    }.catch { error ->
        emit(
            Result.createError(
                label = "Jadwal Praktek",
                body = null,
                error = error
            )
        )
    }
}
