package com.drkempot.data.source.remote.service

import com.drkempot.data.domain.LoginResponse
import com.drkempot.data.domain.RegisterResponse
import com.drkempot.data.source.remote.payload.LoginRequest
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.Multipart
import retrofit2.http.POST

interface AuthService {

    @POST("api/register")
    suspend fun register(
        @Body registerRequest: RequestBody
    ): RegisterResponse

    @POST("api/login")
    suspend fun login(
        @Body loginRequest: LoginRequest
    ): LoginResponse
}
