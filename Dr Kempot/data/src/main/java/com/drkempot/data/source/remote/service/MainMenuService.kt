package com.drkempot.data.source.remote.service

import MainMenuResponse
import retrofit2.http.GET

interface MainMenuService {

    @GET("api/mainmenu")
    suspend fun getMainMenu(): MainMenuResponse
    
}
