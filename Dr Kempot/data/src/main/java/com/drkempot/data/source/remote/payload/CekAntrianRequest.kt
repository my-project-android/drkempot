package com.drkempot.data.source.remote.payload

data class CekAntrianRequest(
    var tanggal: String,
    var user_id: String
)