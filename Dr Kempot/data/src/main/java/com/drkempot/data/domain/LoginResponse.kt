package com.drkempot.data.domain

data class LoginResponse(
    var message: String,
    var access_token: String,
    var token_type: String
)