package com.drkempot.data.domain

data class JadwalPraktekResponse(
    var message: String,
    var jadwalpraktek: ItemJadwalPraktekResp
)

data class ItemJadwalPraktekResp(
    var id: String,
    var keterangan: String,
    var created_at: String,
    var updated_at: String
)