package com.drkempot.data.utils

object KeyPrefConst {
    const val PREF_NAME = "drkempot-app"
    const val JWT_TOKEN_KEY = "JWT_TOKEN"
    const val IS_LOGGED_IN = "IS_LOGGED_IN"
}
