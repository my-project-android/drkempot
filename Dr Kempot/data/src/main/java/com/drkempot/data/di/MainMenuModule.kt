package com.drkempot.data.di

import com.drkempot.data.repository.implementation.MainMenuRepositoryImpl
import com.drkempot.data.repository.interfaces.MainMenuRepository
import com.drkempot.data.source.local.PreferenceHelper
import com.drkempot.data.source.remote.service.MainMenuService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object MainMenuModule {

    @Singleton
    @Provides
    fun provideMainMenuRepository(
        service: MainMenuService,
        preferenceHelper: PreferenceHelper
    ): MainMenuRepository {
        return MainMenuRepositoryImpl(service, preferenceHelper)
    }
}
