package com.drkempot.data.repository.interfaces

import com.drkempot.data.domain.LoginResponse
import com.drkempot.data.domain.RegisterResponse
import com.drkempot.data.source.remote.payload.LoginRequest
import com.drkempot.data.source.remote.service.Result
import kotlinx.coroutines.flow.Flow
import okhttp3.RequestBody

interface AuthRepository {

    fun register(registerRequest: RequestBody):
            Flow<Result<RegisterResponse>>

    fun login(loginRequest: LoginRequest):
            Flow<Result<LoginResponse>>
}
