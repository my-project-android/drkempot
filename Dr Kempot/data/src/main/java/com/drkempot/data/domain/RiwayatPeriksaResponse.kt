package com.drkempot.data.domain

data class RiwayatPeriksaResponse(
    var message: String,
    var riwayatperiksa: List<ItemRiwayatPeriksaResp>?
)

data class ItemRiwayatPeriksaResp(
    var id: String,
    var bpjs: String,
    var user_id: String,
    var tanggal: String,
    var created_at: String,
    var updated_at: String
)