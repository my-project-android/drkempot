package com.drkempot.data.source.remote.payload

data class MendaftarPeriksaRequest(
    var user_id: String,
    var tanggal: String,
    var noantrian: String
)