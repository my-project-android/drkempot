package com.drkempot.data.source.remote.service

import com.drkempot.data.domain.CekAntrianResponse
import com.drkempot.data.domain.JadwalPraktekResponse
import com.drkempot.data.domain.MendaftarPeriksaResponse
import com.drkempot.data.domain.RiwayatPeriksaResponse
import com.drkempot.data.source.remote.payload.CekAntrianRequest
import com.drkempot.data.source.remote.payload.MendaftarPeriksaRequest
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query
import retrofit2.http.QueryMap

interface CheckupService {

    @POST("api/cekantrian")
    suspend fun cekAntrian(@Body cekAntrianRequest: CekAntrianRequest):
            CekAntrianResponse

    @POST("api/mendaftarperiksa")
    suspend fun mendaftarPeriksa(@Body mendaftarPeriksaRequest: MendaftarPeriksaRequest):
            MendaftarPeriksaResponse

    @GET("api/riwayatperiksa")
    suspend fun riwayatPeriksa(@QueryMap queryMap: Map<String, String>):
            RiwayatPeriksaResponse

    @GET("api/jadwalpraktek")
    suspend fun jadwalPraktek():
            JadwalPraktekResponse

}
