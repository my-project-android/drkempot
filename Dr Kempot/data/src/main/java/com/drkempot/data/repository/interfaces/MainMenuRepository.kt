package com.drkempot.data.repository.interfaces

import MainMenuResponse
import com.drkempot.data.source.remote.service.Result
import kotlinx.coroutines.flow.Flow

interface MainMenuRepository {

    fun getMainMenu(): Flow<Result<MainMenuResponse>>

}
