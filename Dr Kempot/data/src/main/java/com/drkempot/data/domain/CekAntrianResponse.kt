package com.drkempot.data.domain

data class CekAntrianResponse(
    var message: String,
    var jumlahantrian: String
)