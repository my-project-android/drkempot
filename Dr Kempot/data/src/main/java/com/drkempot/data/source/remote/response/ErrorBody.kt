package com.drkempot.data.source.remote.response

data class ErrorBody(
    val error: String = "",
)