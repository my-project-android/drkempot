package com.drkempot.data.source.remote.payload

import okhttp3.MultipartBody
import okhttp3.RequestBody

data class RegisterRequest(
    var name: RequestBody,
    var tanggal: RequestBody,
    var ktp: RequestBody,
    var bpjs: RequestBody,
    var alamat: RequestBody,
    var namakepalakeluarga: RequestBody,
    var fotoktp: MultipartBody.Part,
    var fotobpjs: MultipartBody.Part,
    var nowa: String
)