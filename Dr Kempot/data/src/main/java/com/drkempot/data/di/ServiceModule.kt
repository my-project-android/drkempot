package com.drkempot.data.di

import android.content.Context
import com.drkempot.data.source.local.PreferenceHelper
import com.drkempot.data.source.remote.service.AuthService
import com.drkempot.data.source.remote.service.CheckupService
import com.drkempot.data.source.remote.service.MainMenuService
import com.drkempot.data.utils.RetrofitBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ServiceModule {

    @Provides
    @Singleton
    fun providePreferenceHelper(@ApplicationContext context: Context) = PreferenceHelper(context)

    @Singleton
    @Provides
    fun provideAuthService(
        preferenceHelper: PreferenceHelper,
        @ApplicationContext context: Context
    ): AuthService {
        return RetrofitBuilder.build(
            context = context,
            preferenceHelper = preferenceHelper,
            type = AuthService::class.java
        )
    }

    @Singleton
    @Provides
    fun provideMainMenuService(
        preferenceHelper: PreferenceHelper,
        @ApplicationContext context: Context
    ): MainMenuService {
        return RetrofitBuilder.build(
            context = context,
            preferenceHelper = preferenceHelper,
            type = MainMenuService::class.java
        )
    }

    @Singleton
    @Provides
    fun provideCheckupService(
        preferenceHelper: PreferenceHelper,
        @ApplicationContext context: Context
    ): CheckupService {
        return RetrofitBuilder.build(
            context = context,
            preferenceHelper = preferenceHelper,
            type = CheckupService::class.java
        )
    }
}
