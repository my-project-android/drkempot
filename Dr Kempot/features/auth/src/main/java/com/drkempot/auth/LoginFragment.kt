package com.drkempot.auth

import androidx.appcompat.content.res.AppCompatResources
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.viewModels
import com.drkempot.auth.databinding.FragmentLoginBinding
import com.drkempot.core.base.BaseFragment
import com.drkempot.core.utils.navigateInclusivelyToUri
import com.drkempot.core.utils.navigateToUri
import com.drkempot.core.utils.observe
import com.drkempot.data.source.local.PreferenceHelper
import com.drkempot.data.utils.KeyPrefConst
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class LoginFragment :
    BaseFragment<FragmentLoginBinding>(FragmentLoginBinding::inflate) {

    @Inject
    lateinit var preferenceHelper: PreferenceHelper

    private val viewModel: LoginViewModel by viewModels()

    override fun onCreated() {
        viewModel.initState()
        binding.apply {
            toolbar.ivBack.setOnClickListener {
                navigateToUri(com.drkempot.core.R.string.onboard)
            }

            toolbar.tvTitleToolbar.text = getString(com.drkempot.core.R.string.title_login_account)
        }

        setupView()
        setupListener()
        observer()
    }

    private fun setupView() {
        binding.tfNIKLogin.doOnTextChanged { text, _, _, _ ->
            viewModel.StateHandler().setNik(text.toString())
        }
    }

    private fun setupListener() {
        binding.llSelectBirthdayLogin.setOnClickListener {
            val datePicker =
                MaterialDatePicker.Builder.datePicker()
                    .setTitleText("Pilih Tanggal")
                    .build()

            datePicker.show(requireActivity().supportFragmentManager, "tag")

            datePicker.addOnPositiveButtonClickListener {
                viewModel.convertBirthday(it)
            }
            datePicker.addOnCancelListener {
                datePicker.dismiss()
            }
            datePicker.addOnDismissListener {
                datePicker.dismiss()
            }
        }

        binding.btnLoginAccount.setOnClickListener {
            viewModel.RequestHandler().sendData()
        }
    }

    private fun observer() {
        observe(viewModel.convertBirthdayDate) {
            binding.tvBirthdayLogin.text = it
        }
        observe(viewModel.nikValid) {
            if (!binding.tfNIKLogin.isFocusable) {
                val message = it?.let { resourceId -> getString(resourceId) }.orEmpty()
                binding.tfNIKLogin.error = message
            }
        }
        observe(viewModel.isButtonEnabled) {
            binding.btnLoginAccount.isEnabled = it
            if (it) {
                binding.btnLoginAccount.background = AppCompatResources.getDrawable(
                    requireActivity(),
                    com.drkempot.core.R.drawable.button_submit
                ) ?: AppCompatResources.getDrawable(
                    requireActivity(),
                    com.drkempot.core.R.drawable.button_disabled
                )
            }
        }
        observe(viewModel.loginResp) {
            if (it != null) {
                it.access_token.let { jwt -> preferenceHelper.saveJwtSession(jwt) }
                preferenceHelper.putBoolean(KeyPrefConst.IS_LOGGED_IN, true)
                navigateInclusivelyToUri(com.drkempot.core.R.string.mainmenu_route)
            }
        }
        observe(viewModel.error) {
            if (it != "") Snackbar.make(binding.containerLogin, it, Snackbar.LENGTH_SHORT).show()
        }
    }
}

