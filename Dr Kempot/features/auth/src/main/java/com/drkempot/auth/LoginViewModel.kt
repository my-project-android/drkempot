package com.drkempot.auth

import android.annotation.SuppressLint
import androidx.lifecycle.viewModelScope
import com.drkempot.core.base.BaseViewModel
import com.drkempot.core.utils.getNikValidationErrorMessage
import com.drkempot.core.utils.isNikValid
import com.drkempot.data.domain.LoginResponse
import com.drkempot.data.repository.interfaces.AuthRepository
import com.drkempot.data.source.remote.payload.LoginRequest
import com.drkempot.data.source.remote.service.Result
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.Date
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val authRepository: AuthRepository
) : BaseViewModel() {

    private val _nikValid = MutableStateFlow<Int?>(null)
    val nikValid: StateFlow<Int?> = _nikValid

    private val _convertBirthdayDate: MutableStateFlow<String> = MutableStateFlow("")
    var convertBirthdayDate: StateFlow<String> = _convertBirthdayDate

    private val _originalBirthdayDate: MutableStateFlow<String> = MutableStateFlow("")
    var originalBirthdayDate: StateFlow<String> = _originalBirthdayDate

    private val _isButtonEnabled = MutableStateFlow(false)
    val isButtonEnabled: StateFlow<Boolean> = _isButtonEnabled

    private val _loginResp = MutableStateFlow<LoginResponse?>(null)
    val loginResp: StateFlow<LoginResponse?> = _loginResp

    private val _error = MutableStateFlow("")
    val error: StateFlow<String> = _error

    private val nik = MutableStateFlow("")

    fun initState() {
        viewModelScope.launch {
            combine(
                nik,
                convertBirthdayDate
            ) { fieldsArray ->
                _isButtonEnabled.update {
                    areFieldsNotEmpty(fieldsArray)
                }
            }.collect()
        }
    }

    private fun areFieldsNotEmpty(fieldsArray: Array<String>): Boolean {
        return fieldsArray.all { it.isNotEmpty() }
    }

    inner class RequestHandler {
        fun sendData() {
            val loginRequest = LoginRequest(
                ktp = nik.value,
                password = convertBirthdayDate.value
            )

            sendLoginRequest(loginRequest,
                onSuccess = { loginResponse ->
                    _loginResp.value = loginResponse
                },
                onError = { error ->
                    _error.update { error }
                })
        }

        private fun sendLoginRequest(
            loginRequest: LoginRequest,
            onSuccess: (LoginResponse) -> Unit,
            onError: (String) -> Unit
        ) {
            viewModelScope.launch {
                authRepository.login(loginRequest)
                    .onStart { showTransparentLoading() }
                    .onCompletion { dismissTransparentLoading() }
                    .collect { result ->
                        when (result) {
                            is Result.Success -> {
                                result.data?.let { onSuccess(it) }
                            }

                            is Result.Error -> {
                                onError(result.getErrorMessage.orEmpty())
                            }
                        }
                    }
            }
        }
    }

    @SuppressLint("SimpleDateFormat")
    fun convertBirthday(time: Long) {
        val date = Date(time)
        val format = SimpleDateFormat("dd-MM-yyyy")
        _convertBirthdayDate.value = format.format(date)

        val dateOriginal = Date(time)
        val formatOriginal = SimpleDateFormat("yyyy-MM-dd")
        _originalBirthdayDate.value = formatOriginal.format(dateOriginal)
    }

    inner class StateHandler {
        fun setNik(nikNumber: String) {
            nik.update { nikNumber }
            val isValid = nikNumber.run {
                isEmpty() || isNikValid()
            }
            val stringResource = if (isValid) null
            else nikNumber.getNikValidationErrorMessage()
            _nikValid.update { stringResource }
        }
    }
}