package com.drkempot.auth

import android.Manifest
import android.app.Activity
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.provider.MediaStore
import android.widget.Toast
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.viewModels
import com.drkempot.auth.databinding.FragmentRegisterBinding
import com.drkempot.core.base.BaseFragment
import com.drkempot.core.customview.CaptureImageDialogView
import com.drkempot.core.utils.RealPathUtils
import com.drkempot.core.utils.navigateInclusivelyToUri
import com.drkempot.core.utils.navigateToUri
import com.drkempot.core.utils.observe
import com.drkempot.data.source.local.PreferenceHelper
import com.drkempot.data.utils.KeyPrefConst
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import java.io.File
import javax.inject.Inject


@AndroidEntryPoint
class RegisterFragment :
    BaseFragment<FragmentRegisterBinding>(FragmentRegisterBinding::inflate) {

    @Inject
    lateinit var preferenceHelper: PreferenceHelper

    private val viewModel: RegisterViewModel by viewModels()
    private val ktpCaptureImage: CaptureImageDialogView by lazy {
        CaptureImageDialogView(
            requireActivity()
        )
    }

    private val bpjsCaptureImage: CaptureImageDialogView by lazy {
        CaptureImageDialogView(
            requireActivity()
        )
    }

    override fun onCreated() {
        binding.viewmodel = viewModel
        viewModel.initState()
        binding.apply {
            toolbar.ivBack.setOnClickListener {
                navigateToUri(com.drkempot.core.R.string.onboard)
            }

            toolbar.tvTitleToolbar.text =
                getString(com.drkempot.core.R.string.label_register_onboard)
        }

        checkPermission(
            Manifest.permission.CAMERA,
            CAMERA_PERMISSION_CODE
        )

        setupView()
        setupListener()
        observer()
    }

    private fun setupView() {
        binding.tfFullnameRegister.doOnTextChanged { text, _, _, _ ->
            viewModel.StateHandler().setName(text.toString())
        }
        binding.tfNIKRegister.doOnTextChanged { text, _, _, _ ->
            viewModel.StateHandler().setNik(text.toString())
        }
        binding.tfPhoneNumberRegister.doOnTextChanged { text, _, _, _ ->
            viewModel.StateHandler().setPhone(text.toString())
        }
        binding.tfLeadFamilyRegister.doOnTextChanged { text, _, _, _ ->
            viewModel.StateHandler().setLeadFamily(text.toString())
        }
        binding.tfAddressRegister.doOnTextChanged { text, _, _, _ ->
            viewModel.StateHandler().setAddress(text.toString())
        }
        binding.tfNoBpjsRegister.doOnTextChanged { text, _, _, _ ->
            viewModel.StateHandler().setBpjsNumber(text.toString())
        }
    }

    private fun setupListener() {
        binding.llSelectBirthdayRegister.setOnClickListener {
            val datePicker =
                MaterialDatePicker.Builder.datePicker()
                    .setTitleText("Pilih Tanggal")
                    .build()

            datePicker.show(requireActivity().supportFragmentManager, "tag")

            datePicker.addOnPositiveButtonClickListener {
                viewModel.convertBirthday(it)
            }
            datePicker.addOnCancelListener {
                datePicker.dismiss()
            }
            datePicker.addOnDismissListener {
                datePicker.dismiss()
            }
        }

        binding.tvUploadKtpRegister.setOnClickListener {
            ktpCaptureImage.createDialog()
            ktpCaptureImage.openCamera()?.setOnClickListener {
                ktpCaptureImage()
            }
            ktpCaptureImage.openGallery()?.setOnClickListener {
                openKTPGallery()
            }
        }

        binding.tvUploadBpjsRegister.setOnClickListener {
            bpjsCaptureImage.createDialog()
            bpjsCaptureImage.openCamera()?.setOnClickListener {
                bpjsCaptureImage()
            }
            bpjsCaptureImage.openGallery()?.setOnClickListener {
                openBPJSGallery()
            }
        }

        binding.btnRegisterAccount.setOnClickListener {
            viewModel.RequestHandler().sendData()
        }
    }

    private fun observer() {
        observe(viewModel.convertBirthdayDate) {
            binding.tvBirthdayRegister.text = it
        }
        observe(viewModel.nameValid) {
            val message = if (it == false) getString(com.drkempot.core.R.string.error_name) else ""
            if (message != "") Snackbar.make(binding.containerRegister, message, Snackbar.LENGTH_SHORT).show()
        }
        observe(viewModel.nikValid) {
            val message = it?.let { resourceId -> getString(resourceId) } ?: ""
            if (message != "") Snackbar.make(binding.containerRegister, message, Snackbar.LENGTH_SHORT).show()
        }
        observe(viewModel.phoneValid) {
            val message = it?.let { resourceId -> getString(resourceId) } ?: ""
            if (message != "") Snackbar.make(binding.containerRegister, message, Snackbar.LENGTH_SHORT).show()
        }
        observe(viewModel.leadFamilyValid) {
            val message = if (it == false) getString(com.drkempot.core.R.string.error_name) else ""
            if (message != "") Snackbar.make(binding.containerRegister, message, Snackbar.LENGTH_SHORT).show()
        }
        observe(viewModel.addressValid) {
            val message = if (it == false) getString(com.drkempot.core.R.string.error_empty_field) else ""
            if (message != "") Snackbar.make(binding.containerRegister, message, Snackbar.LENGTH_SHORT).show()
        }
        observe(viewModel.isButtonEnabled) {
            binding.btnRegisterAccount.isEnabled = it
            if (it) {
                binding.btnRegisterAccount.background = AppCompatResources.getDrawable(
                    requireActivity(),
                    com.drkempot.core.R.drawable.button_submit
                ) ?: AppCompatResources.getDrawable(
                    requireActivity(),
                    com.drkempot.core.R.drawable.button_disabled
                )
            }
        }
        observe(viewModel.registerResp) {
            if (it != null) {
                // Go to main menu
                it.access_token.let { jwt -> preferenceHelper.saveJwtSession(jwt) }
                preferenceHelper.putBoolean(KeyPrefConst.IS_LOGGED_IN, true)
                navigateInclusivelyToUri(com.drkempot.core.R.string.mainmenu_route)
            }
        }

        observe(viewModel.error) {
            if (it != "") Snackbar.make(binding.containerRegister, it, Snackbar.LENGTH_SHORT).show()
        }
    }

    private fun checkPermission(permission: String, requestCode: Int) {
        if (ContextCompat.checkSelfPermission(
                requireActivity(),
                permission
            ) == PackageManager.PERMISSION_DENIED
        ) {

            ActivityCompat.requestPermissions(requireActivity(), arrayOf(permission), requestCode)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == CAMERA_PERMISSION_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(requireActivity(), "Camera Permission Granted", Toast.LENGTH_SHORT)
                    .show()
            } else {
                Toast.makeText(requireActivity(), "Camera Permission Denied", Toast.LENGTH_SHORT)
                    .show()
            }
        } else {
            if (grantResults.isNotEmpty() && grantResults[0] ==
                PackageManager.PERMISSION_GRANTED
            ) {
                //permission from popup granted
                openKTPGallery()
            } else {
                //permission from popup denied
                Toast.makeText(requireActivity(), "Permission denied", Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }

    private fun openKTPGallery() {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, KTP_PICK_CODE)
    }

    private fun openBPJSGallery() {
        //Intent to pick image
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, BPJS_PICK_CODE)
    }

    private fun ktpCaptureImage() {
        val cameraIntent = Intent(
            MediaStore.ACTION_IMAGE_CAPTURE
        )
        ktpUri = requireActivity().contentResolver.insert(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            ContentValues()
        )
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, ktpUri)
        startActivityForResult(
            Intent.createChooser(cameraIntent, "Select Picture"),
            KTP_REQUEST
        )
    }

    private fun bpjsCaptureImage() {
        val cameraIntent = Intent(
            MediaStore.ACTION_IMAGE_CAPTURE
        )
        bpjsUri = requireActivity().contentResolver.insert(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            ContentValues()
        )
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, bpjsUri)
        startActivityForResult(
            Intent.createChooser(cameraIntent, "Select Picture"),
            BPJS_REQUEST
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == KTP_REQUEST && resultCode == Activity.RESULT_OK) {
            val realPathUtils = RealPathUtils()
            val absolutePath = realPathUtils.getPath(requireContext(), ktpUri!!)
                .toString().removePrefix("/storage/emulated/0/Pictures/")
            binding.tvUploadKtpRegister.text = absolutePath
            val ktpFile = File(realPathUtils.getPath(requireActivity(), ktpUri!!).toString())
            viewModel.StateHandler().setFotoKtp(ktpFile, absolutePath)
        } else if (requestCode == BPJS_REQUEST && resultCode == Activity.RESULT_OK) {
            val bpjsRealPath = RealPathUtils()
            val bpjsAbsolutePath = bpjsRealPath.getPath(requireContext(), bpjsUri!!)
                .toString().removePrefix("/storage/emulated/0/Pictures/")
            binding.tvUploadBpjsRegister.text = bpjsAbsolutePath
            val bpjsFile = File(bpjsRealPath.getPath(requireActivity(), bpjsUri!!).toString())
            viewModel.StateHandler().setFotoBpjs(bpjsFile, bpjsAbsolutePath)
        } else if (resultCode == Activity.RESULT_OK && requestCode == KTP_PICK_CODE) {
            ktpUri = data?.data!!
            val realPathUtils = RealPathUtils()
            val absolutePath = realPathUtils.getPath(requireContext(), ktpUri!!)
                .toString().removePrefix("/storage/emulated/0/Pictures/")
            binding.tvUploadKtpRegister.text = absolutePath
            val ktpFile = File(realPathUtils.getPath(requireActivity(), ktpUri!!).toString())
            viewModel.StateHandler().setFotoKtp(ktpFile, absolutePath)
        } else {
            bpjsUri = data?.data!!
            val bpjsRealPathUtils = RealPathUtils()
            val bpjsAbsolutePath = bpjsRealPathUtils.getPath(requireContext(), bpjsUri!!)
                .toString().removePrefix("/storage/emulated/0/Pictures/")
            binding.tvUploadBpjsRegister.text = bpjsAbsolutePath
            val bpjsFile = File(bpjsRealPathUtils.getPath(requireActivity(), bpjsUri!!).toString())
            viewModel.StateHandler().setFotoBpjs(bpjsFile, bpjsAbsolutePath)
        }

    }

    companion object {
        private const val CAMERA_PERMISSION_CODE = 100
        var ktpUri: Uri? = null
        const val KTP_REQUEST = 1
        var bpjsUri: Uri? = null
        const val BPJS_REQUEST = 2

        //Permission code
        private val PERMISSION_CODE = 1001

        //image pick code
        private val KTP_PICK_CODE = 1000
        private val BPJS_PICK_CODE = 1002
    }
}
