package com.drkempot.auth

import android.annotation.SuppressLint
import androidx.lifecycle.viewModelScope
import com.drkempot.core.base.BaseViewModel
import com.drkempot.core.utils.getNikValidationErrorMessage
import com.drkempot.core.utils.getPhoneValidationErrorMessage
import com.drkempot.core.utils.isNikValid
import com.drkempot.core.utils.isPhoneValid
import com.drkempot.core.utils.isTextOnlyValid
import com.drkempot.data.domain.RegisterResponse
import com.drkempot.data.repository.interfaces.AuthRepository
import com.drkempot.data.source.remote.service.Result
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.text.SimpleDateFormat
import java.util.Date
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor(
    private val authRepository: AuthRepository
) : BaseViewModel() {

    private val _nameValid = MutableStateFlow<Boolean?>(null)
    val nameValid: StateFlow<Boolean?> = _nameValid

    private val _nikValid = MutableStateFlow<Int?>(null)
    val nikValid: StateFlow<Int?> = _nikValid

    private val _phoneValid = MutableStateFlow<Int?>(null)
    val phoneValid: StateFlow<Int?> = _phoneValid

    private val _leadFamilyValid = MutableStateFlow<Boolean?>(null)
    val leadFamilyValid: StateFlow<Boolean?> = _leadFamilyValid

    private val _addressValid = MutableStateFlow<Boolean?>(null)
    val addressValid: StateFlow<Boolean?> = _addressValid

    private val _convertBirthdayDate: MutableStateFlow<String> = MutableStateFlow("")
    var convertBirthdayDate: StateFlow<String> = _convertBirthdayDate

    private val _originalBirthdayDate: MutableStateFlow<String> = MutableStateFlow("")
    var originalBirthdayDate: StateFlow<String> = _originalBirthdayDate

    private val _isButtonEnabled = MutableStateFlow(false)
    val isButtonEnabled: StateFlow<Boolean> = _isButtonEnabled

    private val _registerResp = MutableStateFlow<RegisterResponse?>(null)
    val registerResp: StateFlow<RegisterResponse?> = _registerResp

    private val _error = MutableStateFlow("")
    val error: StateFlow<String> = _error

    private val name = MutableStateFlow("")
    private val nik = MutableStateFlow("")
    private val phone = MutableStateFlow("")
    private val leadFamily = MutableStateFlow("")
    private val address = MutableStateFlow("")
    private val bpjsNumber = MutableStateFlow("")
    private val ktp = MutableStateFlow<File?>(null)
    private val ktpNamePath = MutableStateFlow("")
    private val bpjs = MutableStateFlow<File?>(null)
    private val bpjsNamePath = MutableStateFlow("")

    fun initState() {
        viewModelScope.launch {
            combine(
                name,
                nik,
                phone,
                leadFamily,
                address,
                bpjsNumber
            ) { fieldsArray ->
                _isButtonEnabled.update {
                    areFieldsNotEmpty(fieldsArray)
                }
            }.collect()
        }
    }

    inner class RequestHandler {
        fun sendData() {
            val requestBody: RequestBody = MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("name", name.value)
                .addFormDataPart("tanggal", convertBirthdayDate.value)
                .addFormDataPart("ktp", nik.value)
                .addFormDataPart("bpjs", bpjsNumber.value)
                .addFormDataPart("alamat", address.value)
                .addFormDataPart("namakepalakeluarga", leadFamily.value)
                .addFormDataPart("nowa", phone.value)
                .addFormDataPart(
                    "fotoktp",
                    ktpNamePath.value,
                    RequestBody.create("image/*".toMediaTypeOrNull(), ktp.value!!)
                )
                .addFormDataPart(
                    "fotobpjs",
                    bpjsNamePath.value,
                    RequestBody.create("image/*".toMediaTypeOrNull(), bpjs.value!!)
                )
                .build()

            sendRegister(requestBody,
                onSuccess = { regisResp ->
                    _registerResp.value = regisResp
                },
                onError = { error ->
                    _error.update { error }
                })
        }

        private fun sendRegister(
            registerRequest: RequestBody,
            onSuccess: (RegisterResponse) -> Unit,
            onError: (String) -> Unit
        ) {
            viewModelScope.launch {
                authRepository.register(registerRequest)
                    .onStart { showTransparentLoading() }
                    .onCompletion { dismissTransparentLoading() }
                    .collect { result ->
                        when (result) {
                            is Result.Success -> {
                                result.data?.let { onSuccess(it) }
                            }

                            is Result.Error -> {
                                onError(result.getErrorMessage.orEmpty())
                            }
                        }
                    }
            }
        }
    }

    private fun areFieldsNotEmpty(fieldsArray: Array<String>): Boolean {
        return fieldsArray.all { it.isNotEmpty() }
    }

    @SuppressLint("SimpleDateFormat")
    fun convertBirthday(time: Long) {
        val date = Date(time)
        val format = SimpleDateFormat("dd-MM-yyyy")
        _convertBirthdayDate.value = format.format(date)

        val dateOriginal = Date(time)
        val formatOriginal = SimpleDateFormat("yyyy-MM-dd")
        _originalBirthdayDate.value = formatOriginal.format(dateOriginal)
    }

    inner class StateHandler {
        fun setName(fullName: String) {
            name.update { fullName }
            val isValid = fullName.run {
                isEmpty() || isTextOnlyValid()
            }
            _nameValid.update { isValid }
        }

        fun setPhone(phoneNumber: String) {
            phone.update { phoneNumber }
            val isValid = phoneNumber.run {
                isEmpty() || isPhoneValid()
            }
            val stringResource = if (isValid) null
            else phoneNumber.getPhoneValidationErrorMessage()
            _phoneValid.update { stringResource }
        }

        fun setNik(nikNumber: String) {
            nik.update { nikNumber }
            val isValid = nikNumber.run {
                isEmpty() || isNikValid()
            }
            val stringResource = if (isValid) null
            else nikNumber.getNikValidationErrorMessage()
            _nikValid.update { stringResource }
        }

        fun setLeadFamily(leadFamilyName: String) {
            leadFamily.update { leadFamilyName }
            val isValid = leadFamilyName.run {
                isEmpty() || isTextOnlyValid()
            }
            _leadFamilyValid.update { isValid }
        }

        fun setAddress(addressName: String) {
            address.update { addressName }
            val isValid = addressName.run {
                isEmpty()
            }
            _addressValid.update { isValid }
        }

        fun setBpjsNumber(noBpjs: String) {
            bpjsNumber.update { noBpjs }
        }

        fun setFotoKtp(ktpName: File, ktpPath: String) {
            ktp.update { ktpName }
            ktpNamePath.update { ktpPath }
        }

        fun setFotoBpjs(bpjsName: File, bpjsPath: String) {
            bpjs.update { bpjsName }
            bpjsNamePath.update { bpjsPath }
        }
    }
}