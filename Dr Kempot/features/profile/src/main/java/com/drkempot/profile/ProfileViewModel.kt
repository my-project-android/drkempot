package com.drkempot.profile

import MainMenuResponse
import UserResp
import android.annotation.SuppressLint
import androidx.lifecycle.viewModelScope
import com.drkempot.core.base.BaseViewModel
import com.drkempot.data.repository.interfaces.MainMenuRepository
import com.drkempot.data.source.remote.service.Result
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.Locale
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val mainMenuRepository: MainMenuRepository
): BaseViewModel() {

    private val _getProfile = MutableStateFlow<UserResp?>(null)
    val getProfile: StateFlow<UserResp?> = _getProfile

    private val _birthdayDate = MutableStateFlow("")
    val birthdayDate: StateFlow<String> = _birthdayDate

    private val _error = MutableStateFlow("")
    val error: StateFlow<String> = _error

    fun initState() {
        getProfile()
    }

    private fun getProfile() {
        viewModelScope.launch {
            mainMenuRepository.getMainMenu()
                .onStart { showTransparentLoading() }
                .onCompletion { dismissTransparentLoading() }
                .collect{ result ->
                    when (result) {
                        is Result.Success -> {
                            _getProfile.value = result.data?.user
                        }

                        is Result.Error -> {
                            _error.update { result.message.orEmpty() }
                        }
                    }
                }
        }
    }

    @SuppressLint("SimpleDateFormat")
    fun convertDateWithDay(time: String) {
        val date = SimpleDateFormat("yyyy-MM-dd")
        val format = SimpleDateFormat("dd MMMM yyyy", Locale("id", "ID"))
        _birthdayDate.value = format.format(date.parse(time)!!)
    }
}