package com.drkempot.profile

import androidx.fragment.app.viewModels
import com.drkempot.core.base.BaseFragment
import com.drkempot.core.utils.navigateInclusivelyToUri
import com.drkempot.core.utils.observe
import com.drkempot.data.source.local.PreferenceHelper
import com.drkempot.profile.databinding.FragmentProfileBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ProfileFragment:
    BaseFragment<FragmentProfileBinding>(FragmentProfileBinding::inflate) {

    private val viewModel: ProfileViewModel by viewModels()

    @Inject
    lateinit var preferenceHelper: PreferenceHelper

    override fun onCreated() {
        viewModel.initState()
        binding.apply {
            toolbarprofile.ivBack.setOnClickListener {
                navigateInclusivelyToUri(com.drkempot.core.R.string.mainmenu_route)
            }

            toolbarprofile.tvTitleToolbar.text = getString(com.drkempot.core.R.string.label_title_profile)
        }

        observer()
        setupListener()
    }

    private fun setupListener() {
        binding.btnLogout.setOnClickListener {
            preferenceHelper.clearSessionUserLoginResponse()
            navigateInclusivelyToUri(com.drkempot.core.R.string.onboard)
        }
    }

    private fun observer() {
        observe(viewModel.getProfile) {
            binding.tvInitialNameProfile.text = it?.name?.first().toString()
            binding.tvFullnameProfile.text = it?.name
            binding.tvNumberOfRegistrationProfile.text = it?.id?.subSequence(startRange, endRange)
            binding.tvEmailProfile.text = it?.role

            it?.tanggal.let {tanggal ->
                if (tanggal != null) {
                    viewModel.convertDateWithDay(tanggal)
                }
            }
        }

        observe(viewModel.birthdayDate) {
            binding.tvBirthdayProfile.text = it
        }
    }

    companion object {
        const val startRange: Int = 0
        const val endRange: Int = 7
    }
}
