package com.drkempot.daftarperiksa

import DetailUserResp
import android.annotation.SuppressLint
import androidx.lifecycle.viewModelScope
import com.drkempot.core.base.BaseViewModel
import com.drkempot.data.domain.CekAntrianResponse
import com.drkempot.data.domain.MendaftarPeriksaResponse
import com.drkempot.data.repository.interfaces.CheckupRepository
import com.drkempot.data.repository.interfaces.MainMenuRepository
import com.drkempot.data.source.remote.payload.CekAntrianRequest
import com.drkempot.data.source.remote.payload.MendaftarPeriksaRequest
import com.drkempot.data.source.remote.service.Result
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import javax.inject.Inject

@HiltViewModel
class RegistrationCheckupViewModel @Inject constructor(
    private val checkupRepository: CheckupRepository,
    private val mainMenuRepository: MainMenuRepository
) : BaseViewModel() {

    private val _convertChooseDate: MutableStateFlow<String> = MutableStateFlow("")
    var convertChooseDate: StateFlow<String> = _convertChooseDate

    private val _chooseDate: MutableStateFlow<String> = MutableStateFlow("")
    var chooseDate: StateFlow<String> = _chooseDate

    private val _cekAntrianResp = MutableStateFlow<CekAntrianResponse?>(null)
    val cekAntrianResp: StateFlow<CekAntrianResponse?> = _cekAntrianResp

    private val _userDetailResp = MutableStateFlow<DetailUserResp?>(null)
    val userDetailResp: StateFlow<DetailUserResp?> = _userDetailResp

    private val _daftarPeriksaResp = MutableStateFlow<MendaftarPeriksaResponse?>(null)
    val daftarPeriksaResp: StateFlow<MendaftarPeriksaResponse?> = _daftarPeriksaResp

    private val _error = MutableStateFlow("")
    val error: StateFlow<String> = _error

    private val _isButtonEnabled = MutableStateFlow(false)
    val isButtonEnabled: StateFlow<Boolean> = _isButtonEnabled

    private val _isSameDay = MutableStateFlow(false)
    val isSameDay: StateFlow<Boolean> = _isSameDay

    fun initState() {
        getUserDetail()

        viewModelScope.launch {
            combine(
                chooseDate
            ) { fieldsArray ->
                _isButtonEnabled.update {
                    areFieldsNotEmpty(fieldsArray)
                }
            }.collect()
        }
    }

    private fun areFieldsNotEmpty(fieldsArray: Array<String>): Boolean {
        return fieldsArray.all { it.isNotEmpty() }
    }

    private fun getUserDetail() {
        viewModelScope.launch {
            mainMenuRepository.getMainMenu()
                .onStart { showTransparentLoading() }
                .onCompletion { dismissTransparentLoading() }
                .collect { result ->
                    when (result) {
                        is Result.Success -> {
                            _userDetailResp.value = result.data?.detailuser
                        }

                        is Result.Error -> {
                            _error.value = result.message.toString()
                        }
                    }
                }
        }
    }

    fun cekAntrian(cekAntrianRequest: CekAntrianRequest) {
        viewModelScope.launch {
            checkupRepository.cekAntrian(cekAntrianRequest)
                .onStart { showTransparentLoading() }
                .onCompletion { dismissTransparentLoading() }
                .collect { result ->
                    when (result) {
                        is Result.Success -> {
                            _cekAntrianResp.value = result.data
                            _isSameDay.value = true
                        }

                        is Result.Error -> {
                            _error.value = result.getErrorMessage.orEmpty()
                            _isSameDay.value = false
                        }
                    }
                }
        }
    }

    fun sendDaftarPeriksa(userId: String, tanggal: String, noantrian: String) {
        val mendaftarPeriksaRequest = MendaftarPeriksaRequest(
            user_id = userId,
            tanggal = tanggal,
            noantrian = noantrian
        )

        mendaftarPeriksa(mendaftarPeriksaRequest,
            onSuccess = {
                _daftarPeriksaResp.value = it
            },
            onError = {
                _error.update { it }
            })
    }

    private fun mendaftarPeriksa(
        mendaftarPeriksaRequest: MendaftarPeriksaRequest,
        onSuccess: (MendaftarPeriksaResponse?) -> Unit,
        onError: (String) -> Unit
    ) {
        viewModelScope.launch {
            checkupRepository.mendaftarPeriksa(mendaftarPeriksaRequest)
                .onStart { showTransparentLoading() }
                .onCompletion { dismissTransparentLoading() }
                .collect { result ->
                    when (result) {
                        is Result.Success -> {
                            onSuccess(result.data)
                        }

                        is Result.Error -> {
                            onError(result.getErrorMessage.orEmpty())
                        }
                    }
                }
        }
    }

    @SuppressLint("SimpleDateFormat")
    fun convertChooseDate(time: Long) {
        val date = Date(time)
        val format = SimpleDateFormat("dd-MM-yyyy")
        _convertChooseDate.value = format.format(date)

        val dateOriginal = Date(time)
        val formatOriginal = SimpleDateFormat("yyyy-MM-dd")
        _chooseDate.value = formatOriginal.format(dateOriginal)
    }

    @SuppressLint("SimpleDateFormat")
    fun convertDateWithDay(time: Long) {
        val date = Date(time)
        val format = SimpleDateFormat("EEEE", Locale("id", "ID"))
        _convertChooseDate.value = format.format(date)
    }
}