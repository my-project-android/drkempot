package com.drkempot.daftarperiksa

import androidx.appcompat.content.res.AppCompatResources
import androidx.fragment.app.viewModels
import com.drkempot.core.base.BaseFragment
import com.drkempot.core.utils.navigateInclusivelyToUri
import com.drkempot.core.utils.navigateToUri
import com.drkempot.core.utils.observe
import com.drkempot.daftarperiksa.databinding.FragmentStepOneRegisterCheckBinding
import com.drkempot.data.source.remote.payload.CekAntrianRequest
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class StepOneRegisterCheckFragment
    :
    BaseFragment<FragmentStepOneRegisterCheckBinding>(FragmentStepOneRegisterCheckBinding::inflate) {

    private val viewModel: RegistrationCheckupViewModel by viewModels()

    override fun onCreated() {
        viewModel.initState()

        binding.apply {
            toolbarRegisterStepOne.ivBack.setOnClickListener {
                navigateInclusivelyToUri(com.drkempot.core.R.string.mainmenu_route)
            }
            toolbarRegisterStepOne.tvTitleToolbar.text =
                getString(com.drkempot.core.R.string.label_title_registration_daftar_periksa)
        }
        setupListener()
        observer()
    }

    private fun setupListener() {
        binding.llScheduleCheckRegistration.setOnClickListener {
            val datePicker =
                MaterialDatePicker.Builder.datePicker()
                    .setTitleText("Pilih Tanggal")
                    .build()

            datePicker.show(requireActivity().supportFragmentManager, "tag")

            datePicker.addOnPositiveButtonClickListener {
                viewModel.convertChooseDate(it)
                convertChooseDate = it
            }
            datePicker.addOnCancelListener {
                datePicker.dismiss()
            }
            datePicker.addOnDismissListener {
                datePicker.dismiss()
            }
        }

        binding.btnNextRegistrationCheck.setOnClickListener {
            if (isNotSameDay) {
                navigateToUri(
                    getString(com.drkempot.core.R.string.daftarperiksa_route).replace(
                        "{tanggal}",
                        chooseDate
                    )
                        .replace("{noantrian}", noAntrian.toString())
                        .replace("{jumlahantrian}", jumlahAntrian)
                        .replace("{convertdate}", convertChooseDate.toString())
                )
            }
        }
    }

    private fun observer() {
        observe(viewModel.convertChooseDate) {
            binding.tvChooseDateRegistrationCheckup.text = it
        }

        observe(viewModel.chooseDate) {
            val cekAntrianRequest = CekAntrianRequest(
                tanggal = it,
                user_id = userId
            )
            viewModel.cekAntrian(cekAntrianRequest)
            chooseDate = it
        }

        observe(viewModel.userDetailResp) { detailUser ->
            detailUser?.user_id.let {
                if (it != null) {
                    userId = it
                }
            }
        }

        observe(viewModel.cekAntrianResp) {
            if (it != null) {
                noAntrian = it.jumlahantrian.toInt().plus(1)
                jumlahAntrian = it.jumlahantrian
            }
        }

        observe(viewModel.error) {
            if (it != "") {
                Snackbar.make(
                    binding.containerStepOneRegisterSchedule,
                    getString(com.drkempot.core.R.string.label_error_sameday), Snackbar.LENGTH_SHORT
                ).show()
            }
        }

        observe(viewModel.isButtonEnabled) {
            binding.btnNextRegistrationCheck.isEnabled = it
            if (it) {
                binding.btnNextRegistrationCheck.background = AppCompatResources.getDrawable(
                    requireActivity(),
                    com.drkempot.core.R.drawable.button_submit
                ) ?: AppCompatResources.getDrawable(
                    requireActivity(),
                    com.drkempot.core.R.drawable.button_disabled
                )
            }
        }

        observe(viewModel.isSameDay) {
            isNotSameDay = it
        }
    }

    companion object {
        private var userId: String = ""
        private var noAntrian: Int = 0
        private var isNotSameDay: Boolean = false
        private var chooseDate: String = ""
        private var jumlahAntrian: String = ""
        private var convertChooseDate: Long = 0L
    }
}
