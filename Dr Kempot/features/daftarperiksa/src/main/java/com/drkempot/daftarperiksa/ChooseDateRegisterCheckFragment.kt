package com.drkempot.daftarperiksa

import android.content.res.ColorStateList
import androidx.fragment.app.viewModels
import com.drkempot.core.base.BaseFragment
import com.drkempot.core.utils.navigateInclusivelyToUri
import com.drkempot.core.utils.observe
import com.drkempot.daftarperiksa.databinding.FragmentChooseDateRegistrationCheckBinding
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ChooseDateRegisterCheckFragment
    : BaseFragment<FragmentChooseDateRegistrationCheckBinding>(
    FragmentChooseDateRegistrationCheckBinding::inflate
) {

    private val viewModel: RegistrationCheckupViewModel by viewModels()

    override fun onCreated() {
        viewModel.initState()

        binding.apply {
            toolbar.tvTitleToolbar.text =
                getString(com.drkempot.core.R.string.label_title_registration_daftar_periksa)

            toolbar.ivBack.setOnClickListener {
                navigateInclusivelyToUri(com.drkempot.core.R.string.steponeregistrationcheckup_route)
            }
        }

        tanggal = arguments?.getString("tanggal").orEmpty()
        noantrian = arguments?.getString("noantrian").orEmpty()
        jumlahAntrian = arguments?.getString("jumlahantrian").orEmpty()
        convertDate = arguments?.getString("convertdate").orEmpty()

        viewModel.convertDateWithDay(convertDate.toLong())

        initComponent()
        setupListener()
        observer()
    }

    private fun initComponent() {
        val colorStateList = ColorStateList(
            arrayOf(
                intArrayOf(-android.R.attr.state_enabled),
                intArrayOf(android.R.attr.state_enabled)
            ), intArrayOf(
                resources.getColor(com.drkempot.core.R.color.textColorSecondary),  // disabled
                resources.getColor(com.drkempot.core.R.color.primary) // enabled
            )
        )
        binding.rbChooseDateRegistrationCheck.buttonTintList = colorStateList
        binding.rbChooseDateRegistrationCheck.invalidate()

        binding.rbChooseDateRegistrationCheck.isChecked = true

        binding.tvTotalAntrianRegistrationCheck.text =
            getString(com.drkempot.core.R.string.label_jumlah_antrian, jumlahAntrian)
    }

    private fun setupListener() {
        binding.btnRegisterPeriksa.setOnClickListener {
            viewModel.sendDaftarPeriksa(user_id, tanggal, noantrian)
        }
    }

    private fun observer() {
        observe(viewModel.userDetailResp) {
            it?.user_id.let { id ->
                if (id != null) {
                    user_id = id
                }
            }
        }

        observe(viewModel.daftarPeriksaResp) {
            if (it != null) {
                Snackbar.make(binding.containerDaftarPeriksa, it.message, Snackbar.LENGTH_SHORT)
                    .show()
                navigateInclusivelyToUri(com.drkempot.core.R.string.success_daftarperiksa_route)
            }
        }

        observe(viewModel.error) {
            if (it != "") {
                Snackbar.make(binding.containerDaftarPeriksa, it, Snackbar.LENGTH_SHORT).show()
            }
        }

        observe(viewModel.convertChooseDate) {
            binding.tvChooseDateRegistrationCheck.text = it
        }
    }

    companion object {
        private var tanggal: String = ""
        private var noantrian: String = ""
        private var user_id: String = ""
        private var jumlahAntrian: String = ""
        private var convertDate: String = ""
    }
}