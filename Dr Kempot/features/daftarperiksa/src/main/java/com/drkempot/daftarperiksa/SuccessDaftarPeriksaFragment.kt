package com.drkempot.daftarperiksa

import com.drkempot.core.base.BaseFragment
import com.drkempot.core.utils.navigateInclusivelyToUri
import com.drkempot.daftarperiksa.databinding.FragmentSuccessDaftarPeriksaBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SuccessDaftarPeriksaFragment :
    BaseFragment<FragmentSuccessDaftarPeriksaBinding>(FragmentSuccessDaftarPeriksaBinding::inflate) {

    override fun onCreated() {
        binding.btnBackToMainMenuSuccess.setOnClickListener {
            navigateInclusivelyToUri(com.drkempot.core.R.string.mainmenu_route)
        }
    }
}
