package com.drkempot.mainmenu

import MainMenuResponse
import androidx.lifecycle.viewModelScope
import com.drkempot.core.base.BaseViewModel
import com.drkempot.data.repository.interfaces.MainMenuRepository
import com.drkempot.data.source.remote.service.Result
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainMenuViewModel @Inject constructor(
    private val mainMenuRepository: MainMenuRepository
) : BaseViewModel() {

    private val _mainMenuResp = MutableStateFlow<MainMenuResponse?>(null)
    val mainMenuResp: StateFlow<MainMenuResponse?> = _mainMenuResp

    private val _sisaAntrian = MutableStateFlow(0)
    val sisaAntrian: StateFlow<Int> = _sisaAntrian

    private val _error = MutableStateFlow("")
    val error: StateFlow<String> = _error

    fun initState() {
        getMainMenu()
    }

    private fun getMainMenu() {
        viewModelScope.launch {
            mainMenuRepository.getMainMenu()
                .onStart { showTransparentLoading() }
                .onCompletion { dismissTransparentLoading() }
                .collect { result ->
                    when (result) {
                        is Result.Success -> {
                            _mainMenuResp.value = result.data
                        }

                        is Result.Error -> {
                            _error.update { result.getErrorMessage.orEmpty() }
                        }
                    }
                }
        }
    }

    fun countSisaAntrian(noAntrianUser: Int, noAntrianSekarang: Int) {
        val count = noAntrianUser - noAntrianSekarang
        _sisaAntrian.update { count }
    }
}