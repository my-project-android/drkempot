package com.drkempot.mainmenu.detailantrian

import MainMenuResponse
import android.annotation.SuppressLint
import androidx.lifecycle.viewModelScope
import com.drkempot.core.base.BaseViewModel
import com.drkempot.data.repository.interfaces.MainMenuRepository
import com.drkempot.data.source.remote.service.Result
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.Locale
import javax.inject.Inject

@HiltViewModel
class DetailAntrianViewModel @Inject constructor(
    private val mainMenuRepository: MainMenuRepository
) : BaseViewModel() {

    private val _detailAntrianResp = MutableStateFlow<MainMenuResponse?>(null)
    val detailAntrianResp: StateFlow<MainMenuResponse?> = _detailAntrianResp

    private val _sisaAntrian = MutableStateFlow(0)
    val sisaAntrian: StateFlow<Int> = _sisaAntrian

    private val _tanggalPeriksa = MutableStateFlow("")
    val tanggalPeriksa: StateFlow<String> = _tanggalPeriksa

    private val _error = MutableStateFlow("")
    val error: StateFlow<String> = _error

    fun initState() {
        detailAntrian()
    }

    private fun detailAntrian() {
        viewModelScope.launch {
            mainMenuRepository.getMainMenu()
                .onStart { showTransparentLoading() }
                .onCompletion { dismissTransparentLoading() }
                .collect { result ->
                    when (result) {
                        is Result.Success -> {
                            _detailAntrianResp.value = result.data
                        }

                        is Result.Error -> {
                            _error.update { result.message.orEmpty() }
                        }
                    }
                }
        }
    }

    fun countSisaAntrian(noAntrianUser: Int, noAntrianSekarang: Int) {
        val count = noAntrianUser - noAntrianSekarang
        _sisaAntrian.update { count }
    }

    @SuppressLint("SimpleDateFormat")
    fun convertDateWithDay(time: String) {
        val date = SimpleDateFormat("yyyy-MM-dd")
        val format = SimpleDateFormat("dd MMMM yyyy", Locale("id", "ID"))
        _tanggalPeriksa.value = format.format(date.parse(time)!!)
    }
}
