package com.drkempot.mainmenu.detailantrian

import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.drkempot.core.R
import com.drkempot.core.base.BaseFragment
import com.drkempot.core.utils.navigateInclusivelyToUri
import com.drkempot.core.utils.observe
import com.drkempot.mainmenu.MainMenuFragment
import com.drkempot.mainmenu.databinding.FragmentDetailAntrianBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch

@AndroidEntryPoint
class DetailAntrianFragment :
    BaseFragment<FragmentDetailAntrianBinding>(FragmentDetailAntrianBinding::inflate) {

    private val viewModel: DetailAntrianViewModel by viewModels()

    override fun onCreated() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                while (isActive) kotlin.runCatching {
                    delay(MainMenuFragment.duration)
                    viewModel.initState()
                }
            }
        }

        binding.apply {
            toolbardetailantrian.tvTitleToolbar.text =
                getString(R.string.label_title_detail_pemeriksaan)

            toolbardetailantrian.ivBack.setOnClickListener {
                navigateInclusivelyToUri(R.string.mainmenu_route)
            }
        }

        observer()
    }

    private fun observer() {
        observe(viewModel.detailAntrianResp) {
            if (it?.daftarperiksa != null) {
                binding.tvYourNumberScheduleDetailAntrian.text = it.daftarperiksa.noantrian
                binding.tvNumberOfRegistrationDetailAntrian.text = it.daftarperiksa.id.subSequence(
                    startRange,
                    endRange
                )

                viewModel.convertDateWithDay(it.daftarperiksa.tanggal)
                viewModel.countSisaAntrian(
                    it.daftarperiksa.noantrian.toInt(),
                    it.antriansekarang.number.toInt()
                )
            }
        }

        observe(viewModel.sisaAntrian) {
            binding.tvSisaAntrianDetailAntrian.text =
                getString(R.string.label_number_of_left_schedule, it.toString())
        }

        observe(viewModel.tanggalPeriksa) {
            binding.tvScheduleDateDetailAntrian.text = it
        }
    }

    companion object {
        const val startRange: Int = 0
        const val endRange: Int = 7
    }
}
