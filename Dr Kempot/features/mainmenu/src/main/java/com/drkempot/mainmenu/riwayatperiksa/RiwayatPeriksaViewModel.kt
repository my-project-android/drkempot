package com.drkempot.mainmenu.riwayatperiksa

import android.annotation.SuppressLint
import androidx.lifecycle.viewModelScope
import com.drkempot.core.base.BaseViewModel
import com.drkempot.data.domain.ItemRiwayatPeriksaResp
import com.drkempot.data.domain.RiwayatPeriksaResponse
import com.drkempot.data.repository.interfaces.CheckupRepository
import com.drkempot.data.source.remote.service.Result
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import javax.inject.Inject

@HiltViewModel
class RiwayatPeriksaViewModel @Inject constructor(
    private val checkupRepository: CheckupRepository
) : BaseViewModel() {

    private val _listRiwayatPeriksa = MutableStateFlow(emptyList<ItemRiwayatPeriksaResp>())
    val listRiwayatPeriksa: StateFlow<List<ItemRiwayatPeriksaResp>> = _listRiwayatPeriksa

    private val _filterDateOriginal = MutableStateFlow("")
    val filterDateOriginal: StateFlow<String> = _filterDateOriginal

    private val _filterDate = MutableStateFlow("")
    val filterDate: StateFlow<String> = _filterDate

    private val _error = MutableStateFlow("")
    val error: StateFlow<String> = _error

    private val _isEmpty = MutableStateFlow(false)
    val isEmpty: StateFlow<Boolean> = _isEmpty

    fun initState() {
        getRiwayatPeriksa("")
    }

    fun getRiwayatPeriksa(filter: String) {
        viewModelScope.launch {
            checkupRepository.riwayatPeriksa(filter)
                .onStart { showTransparentLoading() }
                .onCompletion { dismissTransparentLoading() }
                .collect { result ->
                    when (result) {
                        is Result.Success -> {
                            handleData(result.data)
                        }

                        is Result.Error -> {
                            _error.update { result.message.orEmpty() }
                        }
                    }
                }
        }
    }

    private fun handleData(riwayatPeriksaResponse: RiwayatPeriksaResponse?) {
        if (riwayatPeriksaResponse?.riwayatperiksa.isNullOrEmpty()) {
            _isEmpty.value = true
        } else {
            _isEmpty.value = false
            riwayatPeriksaResponse?.riwayatperiksa.let {
                if (!it.isNullOrEmpty()) {
                    _listRiwayatPeriksa.value = it
                }
            }
        }
    }

    @SuppressLint("SimpleDateFormat")
    fun convertDate(time: Long) {
        val date = Date(time)
        val format = SimpleDateFormat("MMMM yyyy", Locale("id", "ID"))
        _filterDate.value = format.format(date)

        val dateOriginal = Date(time)
        val formatOriginal = SimpleDateFormat("yyyy-MM")
        _filterDateOriginal.value = formatOriginal.format(dateOriginal)
    }
}
