package com.drkempot.mainmenu.riwayatperiksa

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.drkempot.data.domain.ItemRiwayatPeriksaResp
import com.drkempot.mainmenu.MainMenuFragment
import com.drkempot.mainmenu.databinding.ItemRiwayatPeriksaBinding
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.util.Date
import java.util.Locale

class AdapterRiwayatPeriksa(private val items: List<ItemRiwayatPeriksaResp>) :
    RecyclerView.Adapter<AdapterRiwayatPeriksa.PeriksaViewHolder>() {

    private lateinit var context: Context

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapterRiwayatPeriksa.PeriksaViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        context = parent.context
        val binding = ItemRiwayatPeriksaBinding.inflate(inflater, parent, false)
        return PeriksaViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: AdapterRiwayatPeriksa.PeriksaViewHolder, position: Int) {
        val itemRiwayatPeriksaResp = items[position]
        holder.tvItemTanggalPeriksa.text = convertDateWithDay(itemRiwayatPeriksaResp.tanggal)
        holder.tvItemNumberOfRegistration.text = itemRiwayatPeriksaResp.id.subSequence(
            RiwayatPeriksaFragment.startRange,
            RiwayatPeriksaFragment.endRange
        )
        holder.tvItemScheduleofKlinik.text =
            context.getString(com.drkempot.core.R.string.label_schedule_klinik)
    }

    inner class PeriksaViewHolder(binding: ItemRiwayatPeriksaBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val tvItemTanggalPeriksa: TextView = binding.tvItemTanggalPeriksa
        val tvItemNumberOfRegistration: TextView = binding.tvItemNumberOfRegistration
        val tvItemScheduleofKlinik: TextView = binding.tvItemScheduleofKlinik
    }

    @SuppressLint("SimpleDateFormat")
    fun convertDateWithDay(time: String): String {
        val date = SimpleDateFormat("yyyy-MM-dd")
        val format = SimpleDateFormat("dd MMMM yyyy", Locale("id", "ID"))
        return format.format(date.parse(time)!!)
    }
}
