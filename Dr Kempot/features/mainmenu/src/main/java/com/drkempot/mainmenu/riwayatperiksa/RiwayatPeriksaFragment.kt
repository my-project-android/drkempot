package com.drkempot.mainmenu.riwayatperiksa

import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.drkempot.core.base.BaseFragment
import com.drkempot.core.utils.navigateInclusivelyToUri
import com.drkempot.core.utils.observe
import com.drkempot.mainmenu.databinding.FragmentRiwayatPeriksaBinding
import com.google.android.material.datepicker.MaterialDatePicker
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RiwayatPeriksaFragment :
    BaseFragment<FragmentRiwayatPeriksaBinding>(FragmentRiwayatPeriksaBinding::inflate) {

    private val viewModel: RiwayatPeriksaViewModel by viewModels()

    override fun onCreated() {
        binding.viewmodel = viewModel
        binding.apply {
            toolbar.tvTitleToolbar.text =
                getString(com.drkempot.core.R.string.label_history_of_checkup)
            toolbar.ivBack.setOnClickListener {
                navigateInclusivelyToUri(com.drkempot.core.R.string.mainmenu_route)
            }
        }

        viewModel.initState()
        observer()
        setupListener()
    }

    private fun setupListener() {
        binding.llFilterDateRiwayatPeriksa.setOnClickListener {
            val datePicker =
                MaterialDatePicker.Builder.datePicker()
                    .setTitleText("Pilih Tanggal")
                    .build()

            datePicker.show(requireActivity().supportFragmentManager, "tag")

            datePicker.addOnPositiveButtonClickListener {
                viewModel.convertDate(it)
            }
            datePicker.addOnCancelListener {
                datePicker.dismiss()
            }
            datePicker.addOnDismissListener {
                datePicker.dismiss()
            }
        }
    }

    private fun observer() {
        observe(viewModel.listRiwayatPeriksa) {
            binding.rvListRiwayatPeriksa.apply {
                this.layoutManager = LinearLayoutManager(requireActivity())
                this.adapter = AdapterRiwayatPeriksa(it)
            }
        }

        observe(viewModel.isEmpty) {
            if (it) {
                binding.emptyRiwayatPeriksa.containerEmptyRiwayatPeriksa.visibility = View.VISIBLE
            } else {
                binding.emptyRiwayatPeriksa.containerEmptyRiwayatPeriksa.visibility = View.GONE
            }
        }

        observe(viewModel.filterDate) {
            if (it != "") {
                binding.tvDateFilterRiwayatPeriksa.text = it
            } else {
                binding.tvDateFilterRiwayatPeriksa.text =
                    getString(com.drkempot.core.R.string.label_filter_riwayat_periksa)
            }
        }

        observe(viewModel.filterDateOriginal) {
            viewModel.getRiwayatPeriksa(it)
        }
    }

    companion object {
        const val startRange: Int = 0
        const val endRange: Int = 7
    }
}
