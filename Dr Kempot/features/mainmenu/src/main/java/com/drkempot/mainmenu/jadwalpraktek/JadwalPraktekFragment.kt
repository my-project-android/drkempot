package com.drkempot.mainmenu.jadwalpraktek

import android.view.View
import androidx.core.text.HtmlCompat
import androidx.fragment.app.viewModels
import com.drkempot.core.base.BaseFragment
import com.drkempot.core.utils.navigateInclusivelyToUri
import com.drkempot.core.utils.observe
import com.drkempot.mainmenu.databinding.FragmentJadwalPraktekBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class JadwalPraktekFragment :
    BaseFragment<FragmentJadwalPraktekBinding>(FragmentJadwalPraktekBinding::inflate) {

    private val viewModel: JadwalPraktekViewModel by viewModels()

    override fun onCreated() {
        viewModel.initState()

        binding.apply {
            toolbarJadwalPraktek.tvTitleToolbar.text =
                getString(com.drkempot.core.R.string.label_title_jadwal_praktek_dokter)

            toolbarJadwalPraktek.ivBack.setOnClickListener {
                navigateInclusivelyToUri(com.drkempot.core.R.string.mainmenu_route)
            }
        }

        observer()
    }

    private fun observer() {
        observe(viewModel.listJadwalPraktek) {
            binding.tvItemJadwalPraktek.text = HtmlCompat.fromHtml(
                it?.jadwalpraktek?.keterangan.toString(),
                HtmlCompat.FROM_HTML_MODE_LEGACY
            )
        }

        observe(viewModel.isEmpty) {
            if (it) {
                binding.emptyJadwalPraktek.containerEmptyJadwalPraktek.visibility = View.VISIBLE
            } else {
                binding.emptyJadwalPraktek.containerEmptyJadwalPraktek.visibility = View.GONE
            }
        }
    }
}
