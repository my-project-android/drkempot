package com.drkempot.mainmenu.jadwalpraktek

import androidx.lifecycle.viewModelScope
import com.drkempot.core.base.BaseViewModel
import com.drkempot.data.domain.JadwalPraktekResponse
import com.drkempot.data.repository.interfaces.CheckupRepository
import com.drkempot.data.source.remote.service.Result
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class JadwalPraktekViewModel @Inject constructor(
    private val checkupRepository: CheckupRepository
) : BaseViewModel() {

    private val _listJadwalPraktek = MutableStateFlow<JadwalPraktekResponse?>(null)
    val listJadwalPraktek: StateFlow<JadwalPraktekResponse?> = _listJadwalPraktek

    private val _isEmpty = MutableStateFlow(false)
    val isEmpty: StateFlow<Boolean> = _isEmpty

    private val _error = MutableStateFlow("")
    val error: StateFlow<String> = _error

    fun initState() {
        jadwalPraktek()
    }

    private fun jadwalPraktek() {
        viewModelScope.launch {
            checkupRepository.jadwalPraktek()
                .onStart { showTransparentLoading() }
                .onCompletion { dismissTransparentLoading() }
                .collect { result ->
                    when (result) {
                        is Result.Success -> {
                            handleData(result.data)
                        }

                        is Result.Error -> {
                            _error.update { result.message.orEmpty() }
                        }
                    }
                }
        }
    }

    private fun handleData(jadwalPraktekResponse: JadwalPraktekResponse?) {
        if (jadwalPraktekResponse?.jadwalpraktek == null) {
            _isEmpty.value = true
        } else {
            _isEmpty.value = false
            _listJadwalPraktek.value = jadwalPraktekResponse
        }
    }
}
