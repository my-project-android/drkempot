package com.drkempot.mainmenu

import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.drkempot.core.base.BaseFragment
import com.drkempot.core.utils.navigateInclusivelyToUri
import com.drkempot.core.utils.observe
import com.drkempot.mainmenu.databinding.FragmentMainMenuBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MainMenuFragment :
    BaseFragment<FragmentMainMenuBinding>(FragmentMainMenuBinding::inflate) {

    private val viewModel: MainMenuViewModel by viewModels()

    override fun onCreated() {
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                while (isActive) kotlin.runCatching {
                    delay(duration)
                    viewModel.initState()
                }
            }
        }
        setupListener()
        observer()
    }

    private fun setupListener() {
        binding.sectionScheduleCheck.tvSeeDetailScheduleCheck.setOnClickListener {
            navigateInclusivelyToUri(com.drkempot.core.R.string.detail_antrian_route)
        }

        binding.sectionScheduleRegistration.setOnClickListener {
            navigateInclusivelyToUri(com.drkempot.core.R.string.steponeregistrationcheckup_route)
        }

        binding.sectionHistoryCheckup.setOnClickListener {
            navigateInclusivelyToUri(com.drkempot.core.R.string.riwayat_periksa_route)
        }

        binding.sectionScheduleOfKlinik.setOnClickListener {
            navigateInclusivelyToUri(com.drkempot.core.R.string.jadwal_praktek_route)
        }

        binding.sectionDaftarPeriksaMainmenu.btnDaftarPeriksaMainMenu.setOnClickListener {
            navigateInclusivelyToUri(com.drkempot.core.R.string.steponeregistrationcheckup_route)
        }

        binding.headerMainmenu.tvInitialNameHome.setOnClickListener {
            navigateInclusivelyToUri(com.drkempot.core.R.string.profile_route)
        }

        binding.sectionScheduleCheck.btnSeeNoAntrian.setOnClickListener {
            navigateInclusivelyToUri(com.drkempot.core.R.string.detail_antrian_route)
        }
    }

    private fun observer() {
        observe(viewModel.mainMenuResp) {
            if (it != null) {
                binding.headerMainmenu.tvFullnameHome.text = it.user.name
                binding.headerMainmenu.tvInitialNameHome.text = it.user.name.first().toString()

                if (it.daftarperiksa != null) {
                    binding.sectionDaftarPeriksaMainmenu.containerDaftarPeriksaMainmenu.visibility = View.GONE
                    binding.sectionScheduleCheck.containerScheduleCheckMenu.visibility = View.VISIBLE

                    binding.sectionScheduleCheck.tvYourNumberScheduleHome.text =
                        it.daftarperiksa.noantrian

                    binding.sectionScheduleCheck.tvNumberOfRegistrationHome.text =
                        it.daftarperiksa.id.subSequence(startRange, endRange)

                    viewModel.countSisaAntrian(
                        it.daftarperiksa.noantrian.toInt(),
                        it.antriansekarang.number.toInt()
                    )
                } else {
                    binding.sectionDaftarPeriksaMainmenu.containerDaftarPeriksaMainmenu.visibility = View.VISIBLE
                    binding.sectionScheduleCheck.containerScheduleCheckMenu.visibility = View.GONE
                }
            }
        }

        observe(viewModel.sisaAntrian) {
            binding.sectionScheduleCheck.tvSisaAntrianHome.text =
                getString(com.drkempot.core.R.string.label_number_of_left_schedule, it.toString())
        }
    }

    companion object {
        const val startRange: Int = 0
        const val endRange: Int = 7
        const val duration: Long = 1000L
    }
}
