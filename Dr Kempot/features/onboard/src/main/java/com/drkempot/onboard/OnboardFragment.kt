package com.drkempot.onboard

import com.drkempot.core.base.BaseFragment
import com.drkempot.core.utils.navigateToUri
import com.drkempot.onboard.databinding.FragmentOnboardBinding

class OnboardFragment:
    BaseFragment<FragmentOnboardBinding>(FragmentOnboardBinding::inflate) {

    override fun onCreated() {
        binding.apply {
            btnRegisterOnboard.setOnClickListener {
                navigateToUri(com.drkempot.core.R.string.register_route)
            }
            btnLoginOnboard.setOnClickListener {
                navigateToUri(com.drkempot.core.R.string.login_route)
            }
        }
    }
}
