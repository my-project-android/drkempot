package com.drkempot.onboard

import android.os.Handler
import com.drkempot.core.base.BaseFragment
import com.drkempot.core.utils.navigateInclusivelyToUri
import com.drkempot.data.source.local.PreferenceHelper
import com.drkempot.data.utils.KeyPrefConst
import com.drkempot.onboard.databinding.FragmentSplashScreenBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SplashScreenFragment :
    BaseFragment<FragmentSplashScreenBinding>(FragmentSplashScreenBinding::inflate) {

    @Inject
    lateinit var preferenceHelper: PreferenceHelper

    override fun onCreated() {
        Handler().postDelayed({
            val isLogin = preferenceHelper.getBoolean(KeyPrefConst.IS_LOGGED_IN)
            if (isLogin) {
                navigateInclusivelyToUri(com.drkempot.core.R.string.mainmenu_route)
            } else {
                navigateInclusivelyToUri(com.drkempot.core.R.string.onboard)
            }
        }, delayMilis)

    }

    companion object {
        const val delayMilis: Long = 3000
    }
}
