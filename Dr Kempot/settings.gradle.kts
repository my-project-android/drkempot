pluginManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
        maven("https://jitpack.io")
    }
}

rootProject.name = "Dr Kempot"
include(":app")
include(":core")
include(":features")
include(":features:onboard")
include(":data")
include(":features:auth")
include(":features:mainmenu")
include(":features:daftarperiksa")
include(":features:profile")
